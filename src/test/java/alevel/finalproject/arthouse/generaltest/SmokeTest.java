package alevel.finalproject.arthouse.generaltest;

import alevel.finalproject.arthouse.pages.createstyle.controller.CreateSaveStyleRoomController;
import alevel.finalproject.arthouse.pages.personalarea.controller.PersonalAreaController;
import alevel.finalproject.arthouse.pages.registration.controller.RegistrationController;
import alevel.finalproject.arthouse.pages.viewstyle.controller.ViewExistStyleController;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
class SmokeTest {
    @Autowired
    private CreateSaveStyleRoomController createSaveStyleRoomController;
    @Autowired
    private PersonalAreaController personalAreaController;
    @Autowired
    private RegistrationController registrationController;
    @Autowired
    private ViewExistStyleController viewExistStyleController;

    @Test
    public void smokeTest() {


        assertThat(createSaveStyleRoomController).isNotNull();
        assertThat(personalAreaController).isNotNull();
        assertThat(registrationController).isNotNull();
        assertThat(viewExistStyleController).isNotNull();
    }

}