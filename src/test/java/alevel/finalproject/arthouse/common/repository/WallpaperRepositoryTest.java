package alevel.finalproject.arthouse.common.repository;

import alevel.finalproject.arthouse.common.entity.*;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
@RunWith(SpringRunner.class)
@DataJpaTest
class WallpaperRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private WallpaperRepository wallpaperRepository;

    @Test
    void getWallpaperByWalls() {

        Wall wall = new Wall();
        Wall wall2 = new Wall();

        KindWallpaper kindWallpaper = new KindWallpaper();
        kindWallpaper.setName("vinil");
        kindWallpaper = entityManager.persist(kindWallpaper);


        Wallpaper wallpaper = new Wallpaper();
        wallpaper.setKindWallpaper(kindWallpaper);
        wallpaper.setRollLength(10d);
        wallpaper.setLinkWallpaper("link");
        wallpaper.setPrice(500d);
        wallpaper.setBrand("brand");
        wallpaper.setRollWidth(1d);
        wallpaper.setName("wallpapaer");
        wallpaper = entityManager.persist(wallpaper);

        User user = new User();
        user.setPassword("password");
        user.setUsername("username");
        user = entityManager.persist(user);

        Room room = new Room();
        room.setCost(300d);
        room.setUser(user);
        room.setArea(40d);
        room.setDate(new Date(System.currentTimeMillis()));
        room.setWalls(List.of());
        room.setGlue(false);
        room.setSaved(false);

        room= entityManager.persist(room);

        wall.setWallpaper(wallpaper);
        wall.setHeight(2d);
        wall.setCost(200d);
        wall.setWidth(5d);
        wall.setRoom(room);
        wall.setNumber(1);
        wall.setNumberRollWallpaper(3d);
        wall.setAreaWallpaper(20d);
        wall.setEmptyAreas(List.of());

        wall2.setWallpaper(wallpaper);
        wall2.setHeight(2d);
        wall2.setCost(200d);
        wall2.setWidth(5d);
        wall2.setRoom(room);
        wall2.setNumber(1);
        wall2.setNumberRollWallpaper(3d);
        wall2.setAreaWallpaper(20d);
        wall2.setEmptyAreas(List.of());

        wall = entityManager.persist(wall);
        wall2 = entityManager.persist(wall2);

        Wallpaper wallpaper1= wallpaperRepository.getWallpaperByWalls(wall);
        assertThat(wallpaper1).isEqualTo(wallpaper);

        Wallpaper wallpaper2= wallpaperRepository.getWallpaperByWalls(wall2);
        assertThat(wallpaper2).isEqualTo(wallpaper);










    }
}