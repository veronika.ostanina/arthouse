package alevel.finalproject.arthouse.common.repository;

import alevel.finalproject.arthouse.common.entity.Glue;
import alevel.finalproject.arthouse.common.entity.KindWallpaper;
import alevel.finalproject.arthouse.common.entity.Wallpaper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
class GlueRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private GlueRepository glueRepository;


    @Test
    void findGlueByKindWallpaper() {
        Glue glue = new Glue();

        glue.setName("test");

        KindWallpaper kindWallpaper = new KindWallpaper();
        kindWallpaper.setName("vinil");

        glue.setQuantityUse(5d);
        glue.setPrice(50d);
        glue.setBrand("brand");
        glue.setWeight(250d);
        glue.setKindWallpaper(entityManager.persist(kindWallpaper));


        entityManager.persist(glue);
        entityManager.flush();

        List<Glue> found = glueRepository.findGlueByKindWallpaper(kindWallpaper);
        assertThat(found.get(0).getName()).isEqualTo(glue.getName());
        assertThat(found.get(0).getPrice()).isEqualTo(glue.getPrice());
        assertThat(found.get(0).getBrand()).isEqualTo(glue.getBrand());
        assertThat(found.get(0).getQuantityUse()).isEqualTo(glue.getQuantityUse());
        assertThat(found.get(0).getQuantityUse()).isEqualTo(glue.getQuantityUse());

    }
}