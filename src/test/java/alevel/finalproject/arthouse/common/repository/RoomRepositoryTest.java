package alevel.finalproject.arthouse.common.repository;

import alevel.finalproject.arthouse.common.entity.Glue;
import alevel.finalproject.arthouse.common.entity.Room;
import alevel.finalproject.arthouse.common.entity.User;
import alevel.finalproject.arthouse.common.security.Role;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
class RoomRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private RoomRepository roomRepository;

    @Test
    void findRoomsByUser() {
        User user = new User();
        user.setPassword("password");
        user.setUsername("username");

        User user2 = new User();
        user2.setPassword("password");
        user2.setUsername("username1");

        user2 = entityManager.persist(user2);
        user = entityManager.persist(user);

        Room room = new Room();
        room.setCost(300d);
        room.setUser(user);
        room.setArea(40d);
        room.setDate(new Date(System.currentTimeMillis()));
        room.setWalls(List.of());
        room.setGlue(false);
        room.setSaved(false);

        Room room2 = new Room();
        room2.setCost(300d);
        room2.setUser(user);
        room2.setArea(40d);
        room2.setDate(new Date(System.currentTimeMillis()));
        room2.setWalls(List.of());
        room2.setGlue(false);
        room2.setSaved(false);

        Room room3 = new Room();
        room3.setCost(300d);
        room3.setUser(user2);
        room3.setArea(40d);
        room3.setDate(new Date(System.currentTimeMillis()));
        room3.setWalls(List.of());
        room3.setGlue(false);
        room3.setSaved(false);

        entityManager.persist(room);
        entityManager.persist(room2);
        entityManager.flush();
        List<Room> found = roomRepository.findRoomsByUser(user);

        assertThat(found.get(0).getArea()).isEqualTo(room.getArea());
        assertThat(found.get(0).getCost()).isEqualTo(room.getCost());
        assertThat(found.get(0).getUser()).isEqualTo(room.getUser());
        assertThat(found.get(0).getWalls()).isEqualTo(room.getWalls());

        assertThat(found.get(1).getArea()).isEqualTo(room2.getArea());
        assertThat(found.get(1).getCost()).isEqualTo(room2.getCost());
        assertThat(found.get(1).getUser()).isEqualTo(room2.getUser());
        assertThat(found.get(1).getWalls()).isEqualTo(room2.getWalls());



    }

}