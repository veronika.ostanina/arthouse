package alevel.finalproject.arthouse.common.repository;

import alevel.finalproject.arthouse.common.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
class UserRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private UserRepository userRepository;

    @Test
    void findUserByUsername() {
        User user = new User();
        user.setPassword("password");
        user.setUsername("username");

        User user2 = new User();
        user2.setPassword("password");
        user2.setUsername("username1");

        user2 = entityManager.persist(user2);
        user = entityManager.persist(user);

        assertThrows(NoSuchElementException.class, ()-> userRepository.findUserByUsername("password").get());
        Optional<User> userFound = userRepository.findUserByUsername("username");
        assertTrue(userFound.isPresent());
        assertThat(userFound.get()).isEqualTo(user);

        Optional<User> userFound2 = userRepository.findUserByUsername("username1");
        assertTrue(userFound2.isPresent());
        assertThat(userFound2.get()).isEqualTo(user2);

    }
}