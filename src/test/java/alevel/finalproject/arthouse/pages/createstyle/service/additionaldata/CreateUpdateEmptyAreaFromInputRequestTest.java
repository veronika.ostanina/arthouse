package alevel.finalproject.arthouse.pages.createstyle.service.additionaldata;

import alevel.finalproject.arthouse.common.entity.EmptyArea;
import alevel.finalproject.arthouse.common.repository.EmptyAreaRepository;
import alevel.finalproject.arthouse.pages.createstyle.service.create.ConvertInputData;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.EmptyAreaRepresent;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
class CreateUpdateEmptyAreaFromInputRequestTest {
    @TestConfiguration
    static class CalculateCostRoomImplTestContextConfig {
        @Autowired
        private   EmptyAreaRepository emptyAreaRepository;
        @Bean
        public CreateUpdateEmptyAreaFromInputRequest CreateUpdateEmptyAreaFromInputRequest() {
            return new CreateUpdateEmptyAreaFromInputRequest( emptyAreaRepository);
        }

    }
    @Autowired
    private CreateUpdateEmptyAreaFromInputRequest createUpdateEmptyAreaFromInputRequest;
    @Test
    void createEmptyArea() {
        List<EmptyAreaRepresent> list = new ArrayList<>();
        for (int i = 0; i <5 ; i++) {
            EmptyAreaRepresent emptyAreaRepresent = new EmptyAreaRepresent();
            emptyAreaRepresent.setHeight(2d);
            emptyAreaRepresent.setLink("link");
            emptyAreaRepresent.setName("name");
            emptyAreaRepresent.setWidth(1d);

           list.add(emptyAreaRepresent);
        }
        List<EmptyArea> emptyAreas = createUpdateEmptyAreaFromInputRequest.createEmptyArea(list);
        emptyAreas.stream().forEach(e-> {
            assertEquals(2d, e.getHeight());
            assertEquals("link", e.getLink());
            assertEquals("name", e.getName());
            assertEquals(1d, e.getWidth());

        });

        List<EmptyArea> emptyAreasEmpty= createUpdateEmptyAreaFromInputRequest.createEmptyArea(List.of());
        assertEquals(List.of(), emptyAreasEmpty);

    }

}