package alevel.finalproject.arthouse.pages.createstyle.service.update;

import alevel.finalproject.arthouse.common.entity.Wall;
import alevel.finalproject.arthouse.common.entity.Wallpaper;
import alevel.finalproject.arthouse.pages.createstyle.service.additionaldata.CreateUpdateEmptyAreaFromInputRequest;
import alevel.finalproject.arthouse.pages.createstyle.service.additionaldata.CreateUpdateWallpaperFromInputRequest;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.WallpaperRepresent;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.wall.WallRepresentAlreadyExistStyle;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.wall.WallRepresentUserStyle;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class UpdateWallTest {


    @Autowired
    private UpdateWall updateWall;
    @MockBean
    private CreateUpdateWallpaperFromInputRequest createUpdateWallpaperFromInputRequest;
    @MockBean
    private CreateUpdateEmptyAreaFromInputRequest createUpdateEmptyAreaFromInputRequest;

    @Before
    public void setUp() {

        Wallpaper wallpaper = new Wallpaper();
        WallpaperRepresent wallpaperRepresent = new WallpaperRepresent();

        Mockito.when(createUpdateWallpaperFromInputRequest.getWallpaper(wallpaper.getIdWallpaper()))
                .thenReturn(wallpaper);
        Mockito.when(createUpdateWallpaperFromInputRequest.updateOwnStyleWallpaper(wallpaperRepresent, wallpaper))
                .thenReturn(wallpaper);
        Mockito.when(createUpdateEmptyAreaFromInputRequest.updateEmptyArea(List.of(), List.of()))
                .thenReturn(List.of());


    }

    @Test
    void updateWalls() {
        Wall wallOld1 = new Wall();
        wallOld1.setNumber(1);

        WallRepresentAlreadyExistStyle wallNew1 = new WallRepresentAlreadyExistStyle();
        WallRepresentUserStyle wallNew2 = new WallRepresentUserStyle();

        wallNew1.setNumber(1);
        wallNew2.setNumber(1);

        wallNew1.setHeight(3d);
        wallNew2.setHeight(3d);
        wallNew1.setWidth(11d);
        wallNew2.setWidth(9d);
        wallNew1.setWallpaper(1L);
        WallpaperRepresent wallpaperRepresent = new WallpaperRepresent();
        wallpaperRepresent.setName("new");
        wallNew2.setWallpaperRepresent(new WallpaperRepresent());


        wallOld1.setHeight(2d);
        wallOld1.setWidth(10d);
        wallOld1.setCost(100D);
        wallOld1.setNumberRollWallpaper(5D);
        wallOld1.setAreaWallpaper(10D);

        Wall wallExistStyle = updateWall.updateWalls(wallOld1, wallNew1);

        assertThat(wallExistStyle.getWidth()).isEqualTo(wallNew1.getWidth());
        assertThat(wallExistStyle.getHeight()).isEqualTo(wallNew1.getHeight());
        assertThat(wallExistStyle.getWidth()).isEqualTo(wallNew1.getWidth());

        Wall wallUserStyle = updateWall.updateWalls(wallOld1, wallNew2);

        assertThat(wallUserStyle.getWidth()).isEqualTo(wallNew2.getWidth());
        assertThat(wallUserStyle.getHeight()).isEqualTo(wallNew2.getHeight());
        assertThat(wallUserStyle.getWidth()).isEqualTo(wallNew2.getWidth());


    }
}