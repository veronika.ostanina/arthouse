package alevel.finalproject.arthouse.pages.createstyle.service.create;

import alevel.finalproject.arthouse.common.entity.*;
import alevel.finalproject.arthouse.common.repository.GlueRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@RunWith(SpringRunner.class)
@DataJpaTest
class CalculateCostRoomTest {


    @TestConfiguration
    static class CalculateCostRoomImplTestContextConfig{
        @Bean
        public CalculateCostRoom calculateCostRoom(){
            return new CalculateCostRoom();
        }
    }

    @Autowired
    private CalculateCostRoom calculateCostRoom;
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private GlueRepository glueRepository;


    @Test
    void calculateRoomCost() {


        Glue glue = new Glue();

        glue.setName("test");

        KindWallpaper kindWallpaper = new KindWallpaper();
        kindWallpaper.setName("vinil");

        glue.setQuantityUse(5d);
        glue.setPrice(50d);
        glue.setBrand("brand");
        glue.setWeight(250d);
        glue.setKindWallpaper(entityManager.persist(kindWallpaper));


        entityManager.persist(glue);


        Wallpaper wallpaper = new Wallpaper();
        wallpaper.setKindWallpaper(kindWallpaper);
        wallpaper.setRollLength(10d);
        wallpaper.setLinkWallpaper("link");
        wallpaper.setPrice(500d);
        wallpaper.setBrand("brand");
        wallpaper.setRollWidth(1d);
        wallpaper.setName("wallpapaer");
        wallpaper = entityManager.persist(wallpaper);


        Wall wall = new Wall();
        Wall wall2 = new Wall();
        User user = new User();
        user.setPassword("password");
        user.setUsername("username");
        user = entityManager.persist(user);

        Room room = new Room();
        room.setCost(300d);
        room.setUser(user);
        room.setArea(40d);
        room.setDate(new Date(System.currentTimeMillis()));
        room.setWalls(List.of());
        room.setGlue(false);
        room.setSaved(false);

        room= entityManager.persist(room);


        wall.setWallpaper(wallpaper);
        wall.setHeight(2d);
        wall.setCost(200d);
        wall.setWidth(5d);
        wall.setRoom(room);
        wall.setNumber(1);
        wall.setNumberRollWallpaper(3d);
        wall.setAreaWallpaper(20d);
        wall.setEmptyAreas(List.of());

        wall2.setWallpaper(wallpaper);
        wall2.setHeight(2d);
        wall2.setCost(200d);
        wall2.setWidth(5d);
        wall2.setRoom(room);
        wall2.setNumber(1);
        wall2.setNumberRollWallpaper(3d);
        wall2.setAreaWallpaper(20d);
        wall2.setEmptyAreas(List.of());

        wall = entityManager.persist(wall);
        wall2 = entityManager.persist(wall2);


        assertEquals(400d,
                calculateCostRoom.calculateRoomCost(List.of(wall, wall2),
                        false, glueRepository));

        assertEquals(200d,
                calculateCostRoom.calculateRoomCost(List.of(wall),
                        false, glueRepository));

        assertEquals(0,
                calculateCostRoom.calculateRoomCost(List.of(),
                        false, glueRepository));

        assertEquals(250d,
                calculateCostRoom.calculateRoomCost(List.of(wall),
                        true, glueRepository));

        assertEquals(500d,
                calculateCostRoom.calculateRoomCost(List.of(wall, wall2),
                        true, glueRepository));

        assertEquals(0,
                calculateCostRoom.calculateRoomCost(List.of(),
                        true, glueRepository));







    }
}