package alevel.finalproject.arthouse.pages.createstyle.service.update;

import alevel.finalproject.arthouse.common.entity.Wall;
import alevel.finalproject.arthouse.common.entity.Wallpaper;
import alevel.finalproject.arthouse.common.repository.WallRepository;
import alevel.finalproject.arthouse.pages.createstyle.service.additionaldata.CreateUpdateEmptyAreaFromInputRequest;
import alevel.finalproject.arthouse.pages.createstyle.service.additionaldata.CreateUpdateWallpaperFromInputRequest;
import alevel.finalproject.arthouse.pages.createstyle.service.create.CalculateNeededQuantityWallpaper;
import alevel.finalproject.arthouse.pages.createstyle.service.create.ConvertInputData;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.WallpaperRepresent;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.wall.WallRepresent;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.wall.WallRepresentAlreadyExistStyle;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.wall.WallRepresentUserStyle;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class DetermineWallsForUpdatingRoomTest {



    @MockBean
    private  UpdateWall updateWall;
    @MockBean
    private CalculateNeededQuantityWallpaper calculateNeededQuantityWallpaper;
    @MockBean
    private WallRepository wallRepository;
    @Autowired
    private DetermineWallsForUpdatingRoom determineWallsForUpdatingRoom;

    @Before
    public void setUp() {

        Wall wallOld1 = new Wall();
        WallRepresentAlreadyExistStyle wallNew1 = new WallRepresentAlreadyExistStyle();

        Mockito.when(updateWall.updateWalls(wallOld1, wallNew1))
                .thenReturn(wallOld1);
        Mockito.doNothing().when(wallRepository).deleteAll();
        Mockito.when(calculateNeededQuantityWallpaper.calculateQuantityMeterWallpaper(List.of(wallOld1)))
                .thenReturn(List.of(wallOld1));

    }

    @Test
    void createSavedWallList() {

        Wall wallOld1 = new Wall();
        Wall wallOld2 = new Wall();
        wallOld1.setNumber(1);
        wallOld2.setNumber(2);

        WallRepresentAlreadyExistStyle wallNew1 = new WallRepresentAlreadyExistStyle();
        WallRepresentAlreadyExistStyle wallNew2 = new WallRepresentAlreadyExistStyle();

        wallNew1.setNumber(1);
        wallNew2.setNumber(2);

        assertThrows(IllegalArgumentException.class, ()->determineWallsForUpdatingRoom.createSavedWallList(List.of(), List.of()));
        assertThrows(IllegalArgumentException.class, ()->determineWallsForUpdatingRoom.createSavedWallList(List.of(wallNew1), List.of()));
        assertThrows(IllegalArgumentException.class, ()->determineWallsForUpdatingRoom.createSavedWallList(List.of(), List.of(wallOld1)));


        List<Wall> walls = determineWallsForUpdatingRoom.createSavedWallList(List.of(wallNew1, wallNew2), List.of(wallOld1, wallOld2));
        assertEquals(2, walls.size());

        List<Wall> walls2 = determineWallsForUpdatingRoom.createSavedWallList(List.of(wallNew1), List.of(wallOld1, wallOld2));
        assertEquals(1, walls2.size());

    }
}