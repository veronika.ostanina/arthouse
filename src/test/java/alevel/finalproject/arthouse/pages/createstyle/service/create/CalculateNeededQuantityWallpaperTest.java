package alevel.finalproject.arthouse.pages.createstyle.service.create;

import alevel.finalproject.arthouse.common.entity.*;
import alevel.finalproject.arthouse.common.repository.GlueRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
@RunWith(SpringRunner.class)
@DataJpaTest
class CalculateNeededQuantityWallpaperTest {


    @TestConfiguration
    static class CalculateCostRoomImplTestContextConfig {
        @Bean
        public CalculateNeededQuantityWallpaper calculateNeededQuantityWallpaper() {
            return new CalculateNeededQuantityWallpaper();
        }
    }

    @Autowired
    private CalculateNeededQuantityWallpaper calculateNeededQuantityWallpaper;
    @Autowired
    private TestEntityManager entityManager;

    @Test
    void calculateQuantityMeterWallpaper() {

        Glue glue = new Glue();

        glue.setName("test");

        KindWallpaper kindWallpaper = new KindWallpaper();
        kindWallpaper.setName("vinil");

        glue.setQuantityUse(5d);
        glue.setPrice(50d);
        glue.setBrand("brand");
        glue.setWeight(250d);
        glue.setKindWallpaper(entityManager.persist(kindWallpaper));


        entityManager.persist(glue);


        Wallpaper wallpaper = new Wallpaper();
        wallpaper.setKindWallpaper(kindWallpaper);
        wallpaper.setRollLength(10d);
        wallpaper.setLinkWallpaper("link");
        wallpaper.setPrice(500d);
        wallpaper.setBrand("brand");
        wallpaper.setRollWidth(1d);
        wallpaper.setName("wallpapaer");
        wallpaper = entityManager.persist(wallpaper);
        Wallpaper wallpaper2 = new Wallpaper();
        wallpaper2.setKindWallpaper(kindWallpaper);
        wallpaper2.setRollLength(10d);
        wallpaper2.setLinkWallpaper("link");
        wallpaper2.setPrice(200d);
        wallpaper2.setBrand("brand");
        wallpaper2.setName("wallpapaer");
        wallpaper2.setRollWidth(1d);
        wallpaper2 = entityManager.persist(wallpaper2);


        User user = new User();
        user.setPassword("password");
        user.setUsername("username");
        user = entityManager.persist(user);

        Room room = new Room();
        room.setCost(300d);
        room.setUser(user);
        room.setArea(40d);
        room.setDate(new Date(System.currentTimeMillis()));
        room.setWalls(List.of());
        room.setGlue(false);
        room.setSaved(false);

        room = entityManager.persist(room);

        Wall wall = new Wall();
        Wall wall2 = new Wall();
        Wall wall3 = new Wall();

        wall.setWallpaper(wallpaper);
        wall.setHeight(2d);
        wall.setWidth(5d);
        wall.setRoom(room);
        wall.setNumber(1);
        wall.setEmptyAreas(List.of());

        wall3.setWallpaper(wallpaper);
        wall3.setHeight(2d);
        wall3.setWidth(4d);
        wall3.setRoom(room);
        wall3.setNumber(3);
        EmptyArea emptyArea = new EmptyArea();
        emptyArea.setName("window");
        emptyArea.setWidth(10d);
        emptyArea.setHeight(10d);
        emptyArea.setLink("link");
        wall3.setEmptyAreas(List.of(entityManager.persist(emptyArea)));

        wall2.setWallpaper(wallpaper2);
        wall2.setHeight(2d);
        wall2.setWidth(12d);
        wall2.setRoom(room);
        wall2.setNumber(2);
        wall2.setEmptyAreas(List.of());


        List<Wall> walls = calculateNeededQuantityWallpaper.calculateQuantityMeterWallpaper(List.of(wall, wall2));


        for (Wall w : walls
        ) {
            if (w.getNumber() == 1) {
                assertThat(w.getAreaWallpaper()).isEqualTo(10);
                assertThat(w.getNumberRollWallpaper()).isEqualTo(1);
                assertThat(w.getCost()).isEqualTo(500);
            } else {

                assertThat(w.getAreaWallpaper()).isEqualTo(24);
                assertThat(w.getNumberRollWallpaper()).isEqualTo(3);
                assertThat(w.getCost()).isEqualTo(600);
            }
        }

        assertThrows(AssertionError.class, ()->calculateNeededQuantityWallpaper.calculateQuantityMeterWallpaper(List.of(wall3)));

        emptyArea.setHeight(1d);
        emptyArea.setWidth(1d);
        wall3.setEmptyAreas(List.of(entityManager.persist((emptyArea))));
        List<Wall> walls2 = calculateNeededQuantityWallpaper.calculateQuantityMeterWallpaper(List.of(wall3));
        assertThat(walls2.get(0).getAreaWallpaper()).isEqualTo(7);
        assertThat(walls2.get(0).getNumberRollWallpaper()).isEqualTo(1);
        assertThat(walls2.get(0).getCost()).isEqualTo(500);




    }
}