package alevel.finalproject.arthouse.pages.createstyle.service.create;

import alevel.finalproject.arthouse.common.entity.Wall;
import alevel.finalproject.arthouse.common.entity.Wallpaper;
import alevel.finalproject.arthouse.common.repository.WallpaperRepository;
import alevel.finalproject.arthouse.pages.createstyle.service.additionaldata.CreateUpdateEmptyAreaFromInputRequest;
import alevel.finalproject.arthouse.pages.createstyle.service.additionaldata.CreateUpdateWallpaperFromInputRequest;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.WallpaperRepresent;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.wall.WallRepresent;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.wall.WallRepresentAlreadyExistStyle;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.wall.WallRepresentUserStyle;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
class ConvertInputDataTest {


    @TestConfiguration
    static class CalculateCostRoomImplTestContextConfig {
        @Autowired
        private  CreateUpdateWallpaperFromInputRequest createUpdateWallpaperFromInputRequest;
        @Autowired
        private  CreateUpdateEmptyAreaFromInputRequest createUpdateEmptyAreaFromInputRequest;
        @Bean
        public ConvertInputData convertInputData() {
            return new ConvertInputData(createUpdateWallpaperFromInputRequest, createUpdateEmptyAreaFromInputRequest);
        }

    }
    @Autowired
    private ConvertInputData convertInputData;
    @MockBean
    private CreateUpdateWallpaperFromInputRequest createUpdateWallpaperFromInputRequest;
    @MockBean
    private  CreateUpdateEmptyAreaFromInputRequest createUpdateEmptyAreaFromInputRequest;


    @Before
    public void setUp() {
        Wallpaper wallpaper = new Wallpaper();
        wallpaper.setIdWallpaper(1l);
        WallpaperRepresent wallpaperRepresent= new WallpaperRepresent();
        WallRepresent wallRepresent = new WallRepresentUserStyle();

        Mockito.when(createUpdateWallpaperFromInputRequest.getWallpaper(wallpaper.getIdWallpaper()))
                .thenReturn(wallpaper);
        Mockito.when(createUpdateWallpaperFromInputRequest.createOwnStyleWallpaper(wallpaperRepresent))
                .thenReturn(wallpaper);
        Mockito.when(createUpdateEmptyAreaFromInputRequest.createEmptyArea(wallRepresent.getEmptyAreaRepresents()))
                .thenReturn(List.of());

    }

    @Test
    void getWall() {
        Wallpaper wallpaper = new Wallpaper();
        wallpaper.setIdWallpaper(1l);
        WallRepresentAlreadyExistStyle representAlreadyExistStyle = new WallRepresentAlreadyExistStyle();

        WallRepresentUserStyle wallRepresentUserStyle = new WallRepresentUserStyle();

        representAlreadyExistStyle.setEmptyAreaRepresents(List.of());
        representAlreadyExistStyle.setHeight(2d);
        representAlreadyExistStyle.setWidth(8d);
        representAlreadyExistStyle.setNumber(1);
        representAlreadyExistStyle.setWallpaper(1l);


        Wall wallCalculated = convertInputData.getWall(representAlreadyExistStyle);

        assertThat(wallCalculated.getEmptyAreas()).isEqualTo(List.of());
        assertThat(wallCalculated.getHeight()).isEqualTo(2d);
        assertThat(wallCalculated.getWidth()).isEqualTo(8d);
        assertThat(wallCalculated.getWidth()).isEqualTo(8d);
        assertThat(wallCalculated.getNumber()).isEqualTo(1);


        wallRepresentUserStyle.setEmptyAreaRepresents(List.of());
        wallRepresentUserStyle.setHeight(2d);
        wallRepresentUserStyle.setWidth(8d);
        wallRepresentUserStyle.setNumber(1);


        Wall wallRepresentUserStyleCalculated = convertInputData.getWall(representAlreadyExistStyle);

        assertThat(wallRepresentUserStyleCalculated.getEmptyAreas()).isEqualTo(List.of());
        assertThat(wallRepresentUserStyleCalculated.getHeight()).isEqualTo(2d);
        assertThat(wallRepresentUserStyleCalculated.getWidth()).isEqualTo(8d);
        assertThat(wallRepresentUserStyleCalculated.getWidth()).isEqualTo(8d);
        assertThat(wallRepresentUserStyleCalculated.getNumber()).isEqualTo(1);




    }
}