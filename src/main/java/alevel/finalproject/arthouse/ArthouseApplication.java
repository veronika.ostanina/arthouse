package alevel.finalproject.arthouse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArthouseApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArthouseApplication.class, args);
    }

}
