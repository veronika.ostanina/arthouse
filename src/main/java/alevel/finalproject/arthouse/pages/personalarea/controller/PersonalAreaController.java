package alevel.finalproject.arthouse.pages.personalarea.controller;

import alevel.finalproject.arthouse.pages.personalarea.service.PersonalAreaService;
import alevel.finalproject.arthouse.pages.personalarea.sourcerepresentation.RoomsRepresentation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@AllArgsConstructor
@RequestMapping("arthouse/rooms")
public class PersonalAreaController {
    private final PersonalAreaService personalAreaService;


    @GetMapping()
    public List<RoomsRepresentation> getRooms(@Autowired Authentication authentication) {
        return personalAreaService.getRooms(authentication);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("delete/{idRoom}")
    public void deleteById(@PathVariable Long idRoom) {
        personalAreaService.deleteRoomById(idRoom);
    }

}
