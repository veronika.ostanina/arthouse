package alevel.finalproject.arthouse.pages.personalarea.sourcerepresentation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoomsRepresentation {
    private Double area;
    private Double cost;
    private Date date;
    private Integer quantityRooms;
    private boolean glue;

}
