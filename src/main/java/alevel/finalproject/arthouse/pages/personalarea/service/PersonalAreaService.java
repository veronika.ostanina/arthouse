package alevel.finalproject.arthouse.pages.personalarea.service;

import alevel.finalproject.arthouse.common.entity.User;
import alevel.finalproject.arthouse.common.exception.MessageStatusNotFoundException;
import alevel.finalproject.arthouse.common.repository.RoomRepository;
import alevel.finalproject.arthouse.common.repository.UserRepository;
import alevel.finalproject.arthouse.common.repository.WallRepository;
import alevel.finalproject.arthouse.pages.personalarea.sourcerepresentation.RoomsRepresentation;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@EnableJpaRepositories
public class PersonalAreaService {

    private final RoomRepository roomRepository;
    private final UserRepository userRepository;
    private final WallRepository wallRepository;

    public PersonalAreaService(RoomRepository roomRepository, UserRepository userRepository, WallRepository wallRepository) {
        this.roomRepository = roomRepository;
        this.userRepository = userRepository;
        this.wallRepository = wallRepository;
    }

    public List<RoomsRepresentation> getRooms(Authentication authentication) {

        Optional<User> user = userRepository.findUserByUsername(authentication.getName());
        assert user.isPresent() : new MessageStatusNotFoundException("User with name " + authentication.getName() + " isn`t exist!");
        return roomRepository.findRoomsByUser(user.get()).stream().collect(
                ArrayList::new,
                (list, room) -> list.add(RoomsRepresentation.builder()
                        .area(room.getArea())
                        .cost(room.getCost())
                        .date(room.getDate())
                        .glue(room.getGlue())
                        .quantityRooms(wallRepository.countWallByRoom(room))
                        .build()),
                List::addAll);

    }


    public void deleteRoomById(Long idRoom) {
        roomRepository.deleteById(idRoom);
    }
}
