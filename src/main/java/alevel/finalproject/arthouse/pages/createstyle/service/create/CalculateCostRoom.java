package alevel.finalproject.arthouse.pages.createstyle.service.create;

import alevel.finalproject.arthouse.common.entity.Glue;
import alevel.finalproject.arthouse.common.entity.Wall;
import alevel.finalproject.arthouse.common.repository.GlueRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

@Component
@NoArgsConstructor
public class CalculateCostRoom {


    public Double calculateRoomCost(List<Wall> walls, boolean isCalculateGlue, @Autowired GlueRepository glueRepository) {
        return walls.stream().mapToDouble(Wall::getCost).sum() + ((isCalculateGlue) ? calculateGlueCost(walls, glueRepository) : 0);

    }

    private Double calculateGlueCost(List<Wall> walls, GlueRepository glueRepository) {
        double finalSum = 0d;
        for (Wall wall : walls) {
            var listGlue = glueRepository.findGlueByKindWallpaper(wall.getWallpaper().getKindWallpaper());
            OptionalDouble lowPrice = listGlue.stream().mapToDouble(Glue::getPrice).min();
            Optional<Glue> glue = listGlue.stream().filter(glueFindOneMin -> glueFindOneMin.getPrice().equals(lowPrice.getAsDouble())).findFirst();
            assert glue.isPresent() : new RuntimeException("that is nothing for glue");
            finalSum += glue.get().getPrice() * Math.ceil(wall.getNumberRollWallpaper() / glue.get().getQuantityUse());
        }
        return finalSum;
    }
}
