package alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.wall;

import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.WallpaperRepresent;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;


@Data
@JsonTypeName("UserWallpaper")
public class WallRepresentUserStyle extends WallRepresent {

    private WallpaperRepresent wallpaperRepresent;


    @Override
    public <T> T getWallpaperData() {
        return (T) wallpaperRepresent;
    }
}
