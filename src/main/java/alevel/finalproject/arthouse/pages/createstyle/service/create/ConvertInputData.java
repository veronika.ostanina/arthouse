package alevel.finalproject.arthouse.pages.createstyle.service.create;

import alevel.finalproject.arthouse.common.entity.Wall;
import alevel.finalproject.arthouse.common.entity.Wallpaper;
import alevel.finalproject.arthouse.pages.createstyle.service.additionaldata.CreateUpdateEmptyAreaFromInputRequest;
import alevel.finalproject.arthouse.pages.createstyle.service.additionaldata.CreateUpdateWallpaperFromInputRequest;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.WallpaperRepresent;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.wall.WallRepresent;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ConvertInputData {


    private final CreateUpdateWallpaperFromInputRequest createUpdateWallpaperFromInputRequest;
    private final CreateUpdateEmptyAreaFromInputRequest createUpdateEmptyAreaFromInputRequest;


    private <T> Wallpaper getWallpaperByEntryType(T entryData) {
        return entryData.getClass().equals(Long.class) ?
                createUpdateWallpaperFromInputRequest.getWallpaper((Long) entryData)
                : createUpdateWallpaperFromInputRequest.createOwnStyleWallpaper((WallpaperRepresent) entryData);
    }


    public Wall getWall(WallRepresent wallRepresent) {
        Wall wall = new Wall();
        wall.setWidth(wallRepresent.getWidth());
        wall.setHeight(wallRepresent.getHeight());
        wall.setNumber(wallRepresent.getNumber());
        wall.setWallpaper(getWallpaperByEntryType(wallRepresent.getWallpaperData()));
        wall.setEmptyAreas(createUpdateEmptyAreaFromInputRequest.createEmptyArea(wallRepresent.getEmptyAreaRepresents()));
        return wall;

    }

}
