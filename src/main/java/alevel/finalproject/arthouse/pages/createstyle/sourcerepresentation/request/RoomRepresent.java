package alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request;


import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.wall.WallRepresent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoomRepresent {

    private List<WallRepresent> walls;
    private boolean glue;
    private Boolean saved;

}
