package alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request;

import lombok.Data;


@Data
public class EmptyAreaRepresent {
    private String name;

    private Double width;

    private Double height;


    private String link;
}
