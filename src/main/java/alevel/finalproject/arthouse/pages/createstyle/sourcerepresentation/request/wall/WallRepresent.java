package alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.wall;

import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.EmptyAreaRepresent;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;

import java.util.List;

@Data
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = WallRepresentAlreadyExistStyle.class, name = "ExistWallpaper"),
        @JsonSubTypes.Type(value = WallRepresentUserStyle.class, name = "UserWallpaper")
})

public abstract class WallRepresent {
    protected Integer number;
    protected Double width;
    protected Double height;
    protected List<EmptyAreaRepresent> emptyAreaRepresents;

    public abstract <T> T getWallpaperData();

}
