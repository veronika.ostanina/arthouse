package alevel.finalproject.arthouse.pages.createstyle.service;

import alevel.finalproject.arthouse.common.entity.KindWallpaper;
import alevel.finalproject.arthouse.common.entity.Wallpaper;
import alevel.finalproject.arthouse.common.repository.KindWallpaperRepository;
import alevel.finalproject.arthouse.common.repository.WallpaperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
@EnableJpaRepositories(basePackages = "alevel.finalproject.arthouse.common.repository")
public class AdditionDataCreatingRoomService {
    private final KindWallpaperRepository kindWallpaperRepository;
    private final WallpaperRepository wallpaperRepository;


    public AdditionDataCreatingRoomService(@Autowired KindWallpaperRepository kindWallpaperRepository, @Autowired WallpaperRepository wallpaperRepository) {
        this.kindWallpaperRepository = kindWallpaperRepository;
        this.wallpaperRepository = wallpaperRepository;
    }

    public List<KindWallpaper> getWallpaperKinds() {
        return kindWallpaperRepository.findAll();
    }

    public Map<Long, String> getWallpapersParameter() {
        return wallpaperRepository.findAll().stream()
                .collect(HashMap::new,
                        (map, wallpaper) -> map.put(wallpaper.getIdWallpaper(), wallpaper.getLinkWallpaper()),
                        HashMap::putAll);
    }

    public Optional<Wallpaper> getWallpaperById(Long id) {
        return wallpaperRepository.findById(id);
    }

}
