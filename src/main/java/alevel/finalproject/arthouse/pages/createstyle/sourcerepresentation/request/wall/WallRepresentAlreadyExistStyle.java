package alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.wall;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Setter;


@JsonTypeName("ExistWallpaper")
public class WallRepresentAlreadyExistStyle extends WallRepresent {
    @Setter
    private Long wallpaper;

    @Override
    public <T> T getWallpaperData() {
        return (T) wallpaper;
    }
}
