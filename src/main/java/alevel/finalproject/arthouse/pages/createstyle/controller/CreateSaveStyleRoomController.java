package alevel.finalproject.arthouse.pages.createstyle.controller;


import alevel.finalproject.arthouse.common.entity.KindWallpaper;
import alevel.finalproject.arthouse.common.entity.Wallpaper;
import alevel.finalproject.arthouse.common.exception.MessageStatusNotFoundException;
import alevel.finalproject.arthouse.pages.createstyle.service.AdditionDataCreatingRoomService;
import alevel.finalproject.arthouse.pages.createstyle.service.CreateRoomService;
import alevel.finalproject.arthouse.pages.createstyle.service.DeleteRoomService;
import alevel.finalproject.arthouse.pages.createstyle.service.UpdateRoomService;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.RoomRepresent;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.response.RoomResponse;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("arthouse/create")
@AllArgsConstructor
public class CreateSaveStyleRoomController {

    private final AdditionDataCreatingRoomService additionDataCreatingRoomService;
    private final CreateRoomService createRoomService;
    private final DeleteRoomService deleteRoomService;
    private final UpdateRoomService updateRoomService;


    @GetMapping("/kinds")
    public List<KindWallpaper> getWallpaperKinds() {
        List<KindWallpaper> kindWallpapers = additionDataCreatingRoomService.getWallpaperKinds();
        assert !kindWallpapers.isEmpty() : new MessageStatusNotFoundException("kind of wallpapers");
        return kindWallpapers;
    }

    @GetMapping("/wallpapers")
    public Map<Long, String> getWallpapersParameter() { //List<Wallpaper>
        Map<Long, String> wallpapers = additionDataCreatingRoomService.getWallpapersParameter();
        assert !wallpapers.isEmpty() : new MessageStatusNotFoundException("wallpapers");
        return wallpapers;
    }

    @GetMapping("/wallpapers/{id}")
    public Wallpaper getWallpaper(@PathVariable Long id) {
        Optional<Wallpaper> wallpaper = additionDataCreatingRoomService.getWallpaperById(id);
        assert wallpaper.isPresent() : new MessageStatusNotFoundException("wallpaper with id " + id);
        return wallpaper.get();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public RoomResponse createRoom(@RequestBody RoomRepresent roomRepresent, @Autowired Authentication authentication) {
        var room = createRoomService.createRoom(roomRepresent, authentication);

        return new RoomResponse(room.getIdRoom(), room.getCost(), room.getArea());
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("delete/room/{idRoom}")
    public void deleteRoom(@PathVariable Long idRoom) {
        deleteRoomService.deleteRoom(idRoom);

    }

    @PutMapping("/room/{id}")
    public RoomResponse updateRoom(@PathVariable Long id, @RequestBody RoomRepresent roomRepresent) {
        var room = updateRoomService.updateRoom(id, roomRepresent);
        return new RoomResponse(room.getIdRoom(), room.getCost(), room.getArea());
    }


}