package alevel.finalproject.arthouse.pages.createstyle.service;

import alevel.finalproject.arthouse.common.entity.Room;
import alevel.finalproject.arthouse.common.entity.Wall;
import alevel.finalproject.arthouse.common.exception.MessageStatusNotFoundException;
import alevel.finalproject.arthouse.common.repository.GlueRepository;
import alevel.finalproject.arthouse.common.repository.RoomRepository;
import alevel.finalproject.arthouse.pages.createstyle.service.create.CalculateCostRoom;
import alevel.finalproject.arthouse.pages.createstyle.service.create.CalculateNeededQuantityWallpaper;
import alevel.finalproject.arthouse.pages.createstyle.service.update.DetermineWallsForUpdatingRoom;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.RoomRepresent;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Component
@Transactional
public class UpdateRoomService {

    private final CalculateCostRoom calculateCostRoom;
    private final DetermineWallsForUpdatingRoom determineWallsForUpdatingRoom;
    private final CalculateNeededQuantityWallpaper calculateNeededQuantityWallpaper;
    private final RoomRepository roomRepository;
    private final GlueRepository glueRepository;

    public UpdateRoomService(CalculateCostRoom calculateCostRoom, DetermineWallsForUpdatingRoom determineWallsForUpdatingRoom, CalculateNeededQuantityWallpaper calculateNeededQuantityWallpaper, RoomRepository roomRepository, GlueRepository glueRepository) {
        this.calculateCostRoom = calculateCostRoom;
        this.determineWallsForUpdatingRoom = determineWallsForUpdatingRoom;
        this.calculateNeededQuantityWallpaper = calculateNeededQuantityWallpaper;
        this.roomRepository = roomRepository;
        this.glueRepository = glueRepository;
    }


    public Room updateRoom(Long id, RoomRepresent roomRepresent) {
        Optional<Room> roomUpdate = roomRepository.findById(id);
        assert roomUpdate.isPresent() : new MessageStatusNotFoundException("Room with id " + id + " isn`t exist!");
        Room room = roomUpdate.get();
        List<Wall> updatedWall = calculateNeededQuantityWallpaper
                .calculateQuantityMeterWallpaper(
                        determineWallsForUpdatingRoom
                                .createSavedWallList(roomRepresent.getWalls(), room.getWalls()));
        room.setWalls(updatedWall);
        room.setArea(updatedWall.stream().mapToDouble(Wall::getAreaWallpaper).sum());
        room.setCost(calculateCostRoom.calculateRoomCost(updatedWall, roomRepresent.isGlue(), glueRepository));
        room.setSaved(roomRepresent.getSaved());
        return roomRepository.save(room);


    }


}
