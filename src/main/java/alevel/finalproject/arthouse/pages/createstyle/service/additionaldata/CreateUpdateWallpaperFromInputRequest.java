package alevel.finalproject.arthouse.pages.createstyle.service.additionaldata;


import alevel.finalproject.arthouse.common.entity.KindWallpaper;
import alevel.finalproject.arthouse.common.entity.Wallpaper;
import alevel.finalproject.arthouse.common.exception.MessageStatusNotFoundException;
import alevel.finalproject.arthouse.common.repository.KindWallpaperRepository;
import alevel.finalproject.arthouse.common.repository.WallpaperRepository;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.WallpaperRepresent;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@AllArgsConstructor
@Component
public class CreateUpdateWallpaperFromInputRequest {
    private final WallpaperRepository wallpaperRepository;
    private final KindWallpaperRepository kindWallpaperRepository;

    public Wallpaper getWallpaper(Long id) {
        Optional<Wallpaper> wallpaper = wallpaperRepository.findById(id);
        assert wallpaper.isPresent() : new MessageStatusNotFoundException("Wallpaper with id" + id);
        return wallpaper.get();
    }

    public Wallpaper updateOwnStyleWallpaper(WallpaperRepresent wallpaperRepresent, Wallpaper wallpaper) {
        return getWallpaper(wallpaperRepresent, wallpaper);
    }


    public Wallpaper createOwnStyleWallpaper(WallpaperRepresent wallpaperRepresent) {
        Wallpaper wallpaper = new Wallpaper();
        return getWallpaper(wallpaperRepresent, wallpaper);
    }

    private Wallpaper getWallpaper(WallpaperRepresent wallpaperRepresent, Wallpaper wallpaper) {
        wallpaper.setPrice(wallpaperRepresent.getPrice());
        wallpaper.setName(wallpaperRepresent.getName());
        wallpaper.setBrand(wallpaperRepresent.getBrand());
        Optional<KindWallpaper> kindWallpaper = kindWallpaperRepository.findById(wallpaperRepresent.getIdKind());
        assert kindWallpaper.isPresent() : new MessageStatusNotFoundException("Kind of wallpaper with id " + wallpaperRepresent.getIdKind() + " isn`t found!");
        wallpaper.setKindWallpaper(kindWallpaper.get());
        wallpaper.setRollLength(wallpaperRepresent.getRollLength());
        wallpaper.setRollWidth(wallpaperRepresent.getRollWidth());
        wallpaper.setLinkWallpaper(wallpaperRepresent.getLinkWallpaper());
        return wallpaperRepository.save(wallpaper);
    }
}
