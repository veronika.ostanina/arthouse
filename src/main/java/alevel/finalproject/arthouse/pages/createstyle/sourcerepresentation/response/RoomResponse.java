package alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RoomResponse {
    private Long IdRoom;
    private Double cost;
    private Double area;
}
