package alevel.finalproject.arthouse.pages.createstyle.service.additionaldata;

import alevel.finalproject.arthouse.common.entity.EmptyArea;
import alevel.finalproject.arthouse.common.repository.EmptyAreaRepository;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.EmptyAreaRepresent;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
@AllArgsConstructor
public class CreateUpdateEmptyAreaFromInputRequest {

    private final EmptyAreaRepository emptyAreaRepository;


    public List<EmptyArea> createEmptyArea(List<EmptyAreaRepresent> emptyAreaRepresents) {
        return emptyAreaRepresents.stream()
                .map((area) -> {
                    EmptyArea emptyArea = new EmptyArea();
                    emptyArea.setHeight(area.getHeight());
                    emptyArea.setLink(area.getLink());
                    emptyArea.setWidth(area.getWidth());
                    emptyArea.setName(area.getName());

                    return emptyAreaRepository.save(emptyArea);
                })
                .collect(toList());

    }

    public List<EmptyArea> updateEmptyArea(List<EmptyAreaRepresent> emptyAreaRepresents, List<EmptyArea> emptyAreasOld) {
        emptyAreaRepository.deleteAll(emptyAreasOld);
        return createEmptyArea(emptyAreaRepresents);

    }


}
