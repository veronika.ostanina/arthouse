package alevel.finalproject.arthouse.pages.createstyle.service.update;

import alevel.finalproject.arthouse.common.entity.Wall;
import alevel.finalproject.arthouse.common.entity.Wallpaper;
import alevel.finalproject.arthouse.common.repository.EmptyAreaRepository;
import alevel.finalproject.arthouse.common.repository.WallRepository;
import alevel.finalproject.arthouse.common.repository.WallpaperRepository;
import alevel.finalproject.arthouse.pages.createstyle.service.additionaldata.CreateUpdateEmptyAreaFromInputRequest;
import alevel.finalproject.arthouse.pages.createstyle.service.additionaldata.CreateUpdateWallpaperFromInputRequest;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.wall.WallRepresent;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
@AllArgsConstructor
public class UpdateWall {

    private final EmptyAreaRepository emptyAreaRepository;
    private final WallpaperRepository wallpaperRepository;
    private final CreateUpdateWallpaperFromInputRequest createUpdateWallpaperFromInputRequest;
    private final CreateUpdateEmptyAreaFromInputRequest createUpdateEmptyAreaFromInputRequest;

    private final WallRepository wallRepository;


    public Wall updateWalls(Wall wallOld, WallRepresent wallRepresent) {
        wallOld.setWidth(wallRepresent.getWidth());
        wallOld.setHeight(wallRepresent.getHeight());
        Wallpaper wallpaper = wallRepresent.getWallpaperData().getClass().equals(Long.class) ?
                createUpdateWallpaperFromInputRequest.getWallpaper(wallRepresent.getWallpaperData()) :
                createUpdateWallpaperFromInputRequest.updateOwnStyleWallpaper(wallRepresent.getWallpaperData(),
                        wallpaperRepository.getWallpaperByWalls(wallOld));
        wallOld.setWallpaper(wallpaper);

        wallOld.setEmptyAreas(createUpdateEmptyAreaFromInputRequest.updateEmptyArea(wallRepresent.getEmptyAreaRepresents(),
                wallOld.getEmptyAreas()));


        return wallRepository.save(wallOld);

    }

}
