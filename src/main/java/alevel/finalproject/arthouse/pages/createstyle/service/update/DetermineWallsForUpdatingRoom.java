package alevel.finalproject.arthouse.pages.createstyle.service.update;

import alevel.finalproject.arthouse.common.entity.Wall;
import alevel.finalproject.arthouse.common.repository.WallRepository;
import alevel.finalproject.arthouse.pages.createstyle.service.create.CalculateNeededQuantityWallpaper;
import alevel.finalproject.arthouse.pages.createstyle.service.create.ConvertInputData;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.wall.WallRepresent;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@AllArgsConstructor
public class DetermineWallsForUpdatingRoom {
    private final ConvertInputData convertInputData;
    private final UpdateWall updateWall;
    private final CalculateNeededQuantityWallpaper calculateNeededQuantityWallpaper;
    private final WallRepository wallRepository;

    public List<Wall> createSavedWallList(List<WallRepresent> wallRepresents, List<Wall> walls) {
        if (wallRepresents.size() == 0 || walls.size() == 0) {
            throw new IllegalArgumentException("Room can`t have 0 walls!");
        }
        int differenceBetweenExistNewValue = walls.size() - wallRepresents.size();
        List<Wall> createdUpdatedNewWalls = new ArrayList<>();
        Map<Integer, WallRepresent> wallsNew = getIntegerWallRepresentMap(wallRepresents);
        Map<Integer, Wall> wallsOld = getIntegerWallMap(walls);

        if (differenceBetweenExistNewValue == 0) {
            wallsNew.forEach((number, wall) -> {
                        createdUpdatedNewWalls.add(updateWall.updateWalls(wallsOld.get(number), wall));

                    }
            );
        } else if (differenceBetweenExistNewValue < 0) {
            wallsOld.forEach((number, wall) -> {
                        createdUpdatedNewWalls.add(updateWall.updateWalls(wall, wallsNew.get(number)));
                        wallsNew.remove(number);
                    }
            );

            createdUpdatedNewWalls.addAll(calculateNeededQuantityWallpaper.calculateQuantityMeterWallpaper(
                    wallsNew.values().stream().collect(ArrayList::new,
                            (list, wall) -> list.add(convertInputData.getWall(wall)),
                            List::addAll)));
        } else {
            wallsNew.forEach((number, wall) -> {
                        createdUpdatedNewWalls.add(updateWall.updateWalls(wallsOld.get(number), wall));
                        wallsOld.remove(number);
                    }
            );
            wallRepository.deleteAll(wallsOld.values());

        }
        return createdUpdatedNewWalls;

    }

    private Map<Integer, WallRepresent> getIntegerWallRepresentMap(List<WallRepresent> wallRepresents) {
        return wallRepresents.stream().collect(
                HashMap::new,
                (map, wall) -> map.put(wall.getNumber(), wall),
                Map::putAll);
    }

    private Map<Integer, Wall> getIntegerWallMap(List<Wall> walls) {
        return walls.stream().collect(
                HashMap::new,
                (map, wall) -> map.put(wall.getNumber(), wall),
                Map::putAll);
    }

}
