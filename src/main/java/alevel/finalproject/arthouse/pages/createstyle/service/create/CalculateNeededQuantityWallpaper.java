package alevel.finalproject.arthouse.pages.createstyle.service.create;

import alevel.finalproject.arthouse.common.entity.EmptyArea;
import alevel.finalproject.arthouse.common.entity.Wall;
import alevel.finalproject.arthouse.common.entity.Wallpaper;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

@Component
@Data
@NoArgsConstructor
public class CalculateNeededQuantityWallpaper {


    public List<Wall> calculateQuantityMeterWallpaper(List<Wall> walls) {
        List<Wall> calculatedWalls = new ArrayList<>();
        Map<Wallpaper, List<Wall>> gropedWallsByWallpaper = walls.stream()
                .collect(groupingBy(Wall::getWallpaper));
        gropedWallsByWallpaper.forEach((wallpaper, listWalls) ->
                calculatedWalls.addAll(calculateWallStyle(listWalls, wallpaper)));
        return calculatedWalls;
    }


    private List<Wall> calculateWallStyle(List<Wall> walls, Wallpaper wallpaper) {
        List<Wall> calculatedWalls = new ArrayList<>();

        for (Wall wall : walls) {

            double areaWithEmptyArea = wall.getHeight() * wall.getWidth() - (wall.getEmptyAreas().isEmpty() ? 0 : calculateAreaEmptyArea(wall.getEmptyAreas()));
            assert areaWithEmptyArea >= 0 : new IllegalArgumentException("That is nothing to calculate, empty area is more than non empty!");

            Double quantity = calculateQuantityRollWallpaper(areaWithEmptyArea, wallpaper.getRollWidth(), wallpaper.getRollLength());
            wall.setAreaWallpaper(areaWithEmptyArea);
            wall.setNumberRollWallpaper(quantity);
            wall.setCost(quantity * wallpaper.getPrice());
            calculatedWalls.add(wall);


        }
        return calculatedWalls;


    }


    private Double calculateAreaEmptyArea(List<EmptyArea> emptyAreas) {
        List<Double> fullArea = emptyAreas.stream().collect(ArrayList::new,
                (list, emptyArea) -> list.add(emptyArea.getHeight() * emptyArea.getWidth()),
                List::addAll);
        return fullArea.stream().reduce(0d, Double::sum);

    }

    private Double calculateQuantityRollWallpaper(Double areaWall, Double widthWallpaper, Double lengthWallpaper) {
        double quantityRoll;
        try {
            quantityRoll = Math.ceil(areaWall / (widthWallpaper * lengthWallpaper));
        } catch (ArithmeticException e) {
            throw new RuntimeException(e);
        }

        return quantityRoll;

    }

}
