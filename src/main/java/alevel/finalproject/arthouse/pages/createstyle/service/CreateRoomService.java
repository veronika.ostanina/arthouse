package alevel.finalproject.arthouse.pages.createstyle.service;


import alevel.finalproject.arthouse.common.entity.Room;
import alevel.finalproject.arthouse.common.entity.User;
import alevel.finalproject.arthouse.common.entity.Wall;
import alevel.finalproject.arthouse.common.exception.MessageStatusNotFoundException;
import alevel.finalproject.arthouse.common.repository.GlueRepository;
import alevel.finalproject.arthouse.common.repository.RoomRepository;
import alevel.finalproject.arthouse.common.repository.UserRepository;
import alevel.finalproject.arthouse.pages.createstyle.service.create.CalculateCostRoom;
import alevel.finalproject.arthouse.pages.createstyle.service.create.CalculateNeededQuantityWallpaper;
import alevel.finalproject.arthouse.pages.createstyle.service.create.ConvertInputData;
import alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request.RoomRepresent;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CreateRoomService {

    private final ConvertInputData convertInputData;
    private final CalculateNeededQuantityWallpaper calculateNeededQuantityWallpaper;
    private final UserRepository userRepository;
    private final RoomRepository roomRepository;
    private final CalculateCostRoom calculateCostRoom;
    private final GlueRepository glueRepository;

    public CreateRoomService(ConvertInputData convertInputData, CalculateNeededQuantityWallpaper calculateNeededQuantityWallpaper, UserRepository userRepository, RoomRepository roomRepository, CalculateCostRoom calculateCostRoom, GlueRepository glueRepository) {
        this.convertInputData = convertInputData;
        this.calculateNeededQuantityWallpaper = calculateNeededQuantityWallpaper;
        this.userRepository = userRepository;
        this.roomRepository = roomRepository;
        this.calculateCostRoom = calculateCostRoom;
        this.glueRepository = glueRepository;
    }

    public Room createRoom(RoomRepresent roomRepresent, Authentication authentication) {
        Room room = new Room();
        List<Wall> walls = calculateNeededQuantityWallpaper.calculateQuantityMeterWallpaper(
                roomRepresent.getWalls().stream().collect(ArrayList::new,
                        (list, wall) -> list.add(convertInputData.getWall(wall)),
                        List::addAll));
        walls.forEach(wall -> wall.setRoom(room));
        room.setWalls(walls);
        room.setDate(new Date(System.currentTimeMillis()));
        room.setSaved(roomRepresent.getSaved());
        Optional<User> user = userRepository.findUserByUsername(authentication.getName());
        assert user.isPresent() : new MessageStatusNotFoundException("User with id " + authentication.getName() + " isn`t exist!");
        room.setUser(user.get());
        room.setArea(walls.stream().mapToDouble(Wall::getAreaWallpaper).sum());
        room.setGlue(roomRepresent.isGlue());
        room.setCost(calculateCostRoom.calculateRoomCost(walls, roomRepresent.isGlue(), glueRepository));

        return roomRepository.save(room);

    }


}
