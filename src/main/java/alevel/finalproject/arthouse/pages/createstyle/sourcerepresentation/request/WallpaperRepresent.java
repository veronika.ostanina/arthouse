package alevel.finalproject.arthouse.pages.createstyle.sourcerepresentation.request;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class WallpaperRepresent {

    private Double price;

    private String name;

    private String brand;

    private Long idKind;

    private Double rollLength;

    private Double rollWidth;

    private String linkWallpaper;

}
