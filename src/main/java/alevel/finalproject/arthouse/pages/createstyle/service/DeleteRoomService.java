package alevel.finalproject.arthouse.pages.createstyle.service;

import alevel.finalproject.arthouse.common.entity.Room;
import alevel.finalproject.arthouse.common.exception.MessageStatusNotFoundException;
import alevel.finalproject.arthouse.common.repository.RoomRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class DeleteRoomService {
    private final RoomRepository roomRepository;

    public DeleteRoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }


    public void deleteRoom(Long idRoom) {

        Optional<Room> roomDelete = roomRepository.findById(idRoom);
        assert roomDelete.isPresent() : new MessageStatusNotFoundException("room with id " + idRoom + " isn`t found ");
        roomRepository.delete(roomDelete.get());

    }

}
