package alevel.finalproject.arthouse.pages.registration.service;


import alevel.finalproject.arthouse.common.entity.EntityRole;
import alevel.finalproject.arthouse.common.entity.User;
import alevel.finalproject.arthouse.common.repository.RoleRepository;
import alevel.finalproject.arthouse.common.security.Role;
import alevel.finalproject.arthouse.pages.registration.sourcerepresentention.UserRepresentation;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class RegistrationService {
    private final RoleRepository roleRepository;


    public User createUser(UserRepresentation userRepresentation) {

        User user = new User();
        user.setUsername(userRepresentation.getNickname());
        user.setPassword(new BCryptPasswordEncoder().encode(userRepresentation.getPassword()));
        EntityRole role = new EntityRole(Role.USER);
        role.setUser(user);

        return roleRepository.save(role).getUser();

    }

}
