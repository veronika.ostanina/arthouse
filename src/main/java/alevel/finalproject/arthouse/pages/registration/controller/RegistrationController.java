package alevel.finalproject.arthouse.pages.registration.controller;

import alevel.finalproject.arthouse.pages.registration.service.RegistrationService;
import alevel.finalproject.arthouse.pages.registration.sourcerepresentention.UserRepresentation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/registr")
public class RegistrationController {
    private final RegistrationService registrationService;

    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public String createUser(@RequestBody UserRepresentation userRepresentation) {
        return registrationService.createUser(userRepresentation).getUsername();
    }
}
