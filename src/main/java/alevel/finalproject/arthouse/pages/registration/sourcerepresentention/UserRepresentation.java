package alevel.finalproject.arthouse.pages.registration.sourcerepresentention;

import lombok.Data;

@Data
public class UserRepresentation {
    private String nickname;
    private String password;

}
