package alevel.finalproject.arthouse.pages.viewstyle.controller;

import alevel.finalproject.arthouse.pages.viewstyle.service.ViewExistStyleService;
import alevel.finalproject.arthouse.pages.viewstyle.sourcerepresentation.RoomRepresentResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("arthouse/exist")
@AllArgsConstructor
public class ViewExistStyleController {
    private final ViewExistStyleService viewExistStyleService;

    @GetMapping("/rooms/{quantity}")
    public List<RoomRepresentResponse> getLastTwentyRooms(@PathVariable Integer quantity) {
        return viewExistStyleService.getRooms(quantity);
    }


}
