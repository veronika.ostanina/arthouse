package alevel.finalproject.arthouse.pages.viewstyle.sourcerepresentation;

import alevel.finalproject.arthouse.common.entity.Room;
import lombok.Data;

@Data
public class RoomRepresentResponse {
    private Long idRoom;
    private Double costRoom;
    private Double areaRoom;
    private Boolean isGlue;

    public RoomRepresentResponse(Room room) {
        idRoom = room.getIdRoom();
        costRoom = room.getCost();
        areaRoom = room.getArea();
        isGlue = room.getGlue();

    }
}
