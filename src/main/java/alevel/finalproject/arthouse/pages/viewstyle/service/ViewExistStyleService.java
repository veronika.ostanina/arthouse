package alevel.finalproject.arthouse.pages.viewstyle.service;

import alevel.finalproject.arthouse.common.repository.RoomRepository;
import alevel.finalproject.arthouse.pages.viewstyle.sourcerepresentation.RoomRepresentResponse;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
@AllArgsConstructor
public class ViewExistStyleService {

    private final RoomRepository roomRepository;

    public List<RoomRepresentResponse> getRooms(Integer quantity) {
        return roomRepository
                .findAll(PageRequest.of(0, quantity))
                .get()
                .map(RoomRepresentResponse::new)
                .collect(toList());


    }
}

