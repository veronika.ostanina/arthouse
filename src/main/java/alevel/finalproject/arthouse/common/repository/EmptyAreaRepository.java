package alevel.finalproject.arthouse.common.repository;

import alevel.finalproject.arthouse.common.entity.EmptyArea;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmptyAreaRepository extends JpaRepository<EmptyArea, Long> {
}
