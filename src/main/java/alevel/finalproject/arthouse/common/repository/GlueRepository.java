package alevel.finalproject.arthouse.common.repository;

import alevel.finalproject.arthouse.common.entity.Glue;
import alevel.finalproject.arthouse.common.entity.KindWallpaper;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GlueRepository extends JpaRepository<Glue, Long> {
    List<Glue> findGlueByKindWallpaper(KindWallpaper kind);
}
