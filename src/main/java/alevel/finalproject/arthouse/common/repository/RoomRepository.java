package alevel.finalproject.arthouse.common.repository;

import alevel.finalproject.arthouse.common.entity.Room;
import alevel.finalproject.arthouse.common.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoomRepository extends JpaRepository<Room, Long> {
    List<Room> findRoomsByUser(User user);

    Page<Room> findAll(Pageable page);

}
