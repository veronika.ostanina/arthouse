package alevel.finalproject.arthouse.common.repository;

import alevel.finalproject.arthouse.common.entity.KindWallpaper;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KindWallpaperRepository extends JpaRepository<KindWallpaper, Long> {

}
