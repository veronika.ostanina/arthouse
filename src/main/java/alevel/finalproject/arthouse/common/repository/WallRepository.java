package alevel.finalproject.arthouse.common.repository;


import alevel.finalproject.arthouse.common.entity.Room;
import alevel.finalproject.arthouse.common.entity.Wall;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WallRepository extends JpaRepository<Wall, Long> {

    Integer countWallByRoom(Room room);
}