package alevel.finalproject.arthouse.common.repository;

import alevel.finalproject.arthouse.common.entity.Wall;
import alevel.finalproject.arthouse.common.entity.Wallpaper;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WallpaperRepository extends JpaRepository<Wallpaper, Long> {
    Wallpaper getWallpaperByWalls(Wall wall);
}
