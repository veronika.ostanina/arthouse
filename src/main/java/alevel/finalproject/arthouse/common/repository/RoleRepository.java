package alevel.finalproject.arthouse.common.repository;

import alevel.finalproject.arthouse.common.entity.EntityRole;
import alevel.finalproject.arthouse.common.security.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<EntityRole, Long> {
    Boolean existsEntityRoleByRole(Role role);

    EntityRole getEntityRoleByRole(Role role);
}
