package alevel.finalproject.arthouse.common.security;

import alevel.finalproject.arthouse.common.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@EnableJpaRepositories
public class UserService implements UserDetailsService {


    private final UserRepository userRepository;


    public UserService(@Autowired UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findUserByUsername(s).orElseThrow(() -> new UsernameNotFoundException("User with username" + s + " is not found!"));

    }
}
