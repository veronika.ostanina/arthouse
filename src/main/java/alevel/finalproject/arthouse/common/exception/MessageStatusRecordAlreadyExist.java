package alevel.finalproject.arthouse.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class MessageStatusRecordAlreadyExist extends ResponseStatusException {
    public MessageStatusRecordAlreadyExist(String idRecord) {
        super(HttpStatus.FORBIDDEN, idRecord);
    }
}
