package alevel.finalproject.arthouse.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class MessageStatusNotFoundException extends ResponseStatusException {

    public MessageStatusNotFoundException(String names) {
        super(HttpStatus.NOT_FOUND, names + " isn`t exist !");
    }


}
