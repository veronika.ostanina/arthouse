package alevel.finalproject.arthouse.common.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = "glues")
public class Glue {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idGlue;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "brand", nullable = false)
    private String brand;
    @Column(name = "price", nullable = false)
    private Double price;
    @Column(name = "quantityUse", nullable = false)
    private Double quantityUse;
    @Column(name = "weight", nullable = false)
    private Double weight;
    @ManyToOne
    @JoinColumn(name = "kind_wallpaper_glue")
    private KindWallpaper kindWallpaper;

}
