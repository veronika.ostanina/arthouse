package alevel.finalproject.arthouse.common.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "walls")
public class Wall {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idWall;

    @Column(name = "width", nullable = false)
    private Double width;

    @Column(name = "height", nullable = false)
    private Double height;
    @Column(name = "number", nullable = false)
    private Integer number;

    @Column(name = "areaWallpaper", nullable = false)
    private Double areaWallpaper;

    @Column(name = "numberRollWallpaper", nullable = false)
    private Double numberRollWallpaper;
    @Column(name = "cost", nullable = false)
    private Double cost;

    @ManyToOne
    @JoinColumn(name = "wallpaper_wall")
    private Wallpaper wallpaper;

    @ManyToOne()
    @JoinColumn(name = "wall_room")
    private Room room;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "Wall_Empty_area",
            joinColumns = @JoinColumn(name = "idWall"),
            inverseJoinColumns = @JoinColumn(name = "idEmptyArea")
    )
    private List<EmptyArea> emptyAreas;


}
