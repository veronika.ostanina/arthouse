package alevel.finalproject.arthouse.common.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "wallpapers")
public class Wallpaper {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idWallpaper;

    @Column(name = "price", nullable = false)
    private Double price;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "brand", nullable = false)
    private String brand;


    @Column(name = "rollLength", nullable = false)
    private Double rollLength;

    @Column(name = "rollWidth", nullable = false)
    private Double rollWidth;

    @Column(name = "linkWallpaper", nullable = false)
    private String linkWallpaper;

    @ManyToOne
    @JoinColumn(name = "kind_wallpaper_wallpaper")
    private KindWallpaper kindWallpaper;

    @OneToMany(mappedBy = "wallpaper", cascade = CascadeType.REFRESH)
    private List<Wall> walls = new ArrayList<>();


}
