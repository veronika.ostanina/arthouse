package alevel.finalproject.arthouse.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "empty_areas")
public class EmptyArea {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEmptyArea;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "width", nullable = false)
    private Double width;

    @Column(name = "height_side", nullable = false)
    private Double height;


    @Column(name = "link", nullable = false)
    private String link;

    @ManyToMany(mappedBy = "emptyAreas")
    private List<Wall> walls;


}
