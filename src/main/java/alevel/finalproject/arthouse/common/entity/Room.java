package alevel.finalproject.arthouse.common.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "rooms")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idRoom;

    @Column(name = "area", nullable = false)
    private Double area;

    @Column(name = "cost", nullable = false)
    private Double cost;

    @Column(name = "date", nullable = false)
    private Date date;

    @Column(name = "saved", nullable = false, columnDefinition = "boolean default false")
    private Boolean saved;
    @Column(name = "glue", nullable = false, columnDefinition = "boolean default false")
    private Boolean glue;

    @ManyToOne
    @JoinColumn(name = "user_room")
    private User user;

    @OneToMany(mappedBy = "room", cascade = CascadeType.ALL)
    private List<Wall> walls = new ArrayList<>();


}
