package alevel.finalproject.arthouse.common.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "kinds_wallpaper")
public class KindWallpaper {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idKindWallpaper;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "kindWallpaper", cascade = CascadeType.ALL)
    private List<Glue> glues = new ArrayList<>();


    @OneToMany(mappedBy = "kindWallpaper", cascade = CascadeType.ALL)
    private List<Wallpaper> wallpapers = new ArrayList<>();

}
