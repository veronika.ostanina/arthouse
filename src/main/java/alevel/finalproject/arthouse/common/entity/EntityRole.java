package alevel.finalproject.arthouse.common.entity;

import alevel.finalproject.arthouse.common.security.Role;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "roles")
public class EntityRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idRole;

    @Enumerated(EnumType.STRING)
    @Column(unique = true)
    private Role role;


    @ManyToOne(cascade = CascadeType.ALL)
    private User user;


    public EntityRole() {
    }

    public EntityRole(Role role) {
        this.role = role;
    }
}